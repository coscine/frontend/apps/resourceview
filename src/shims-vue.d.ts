declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module '@coscine/api-connection';
declare module '@coscine/app-util';
declare module '@coscine/form-generator';
declare module '@ungap/url-search-params';

declare module 'vue-loading-overlay';

declare module 'vue2-dropzone';

declare module "*.png" {
  const value: any;
  export default value;
}

