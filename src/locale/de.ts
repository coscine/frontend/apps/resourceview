export default {
    resources: 'Ressourcen',

    upload: 'Hochladen',
    download: 'Herunterladen',

    canDropFile: 'Sie können Ihre Datei hier ablegen!',

    loading: 'Laden...',
    typeToSearch: 'Zur Suche tippen',
    clear: 'Leeren',
    perPage: 'Pro Seite',
    more: 'Mehr',
    fileName: 'Dateiname',
    lastModified: 'Zuletzt modifiziert',
    actions: 'Aktionen',

    noData: 'Diese Ressource enthält keine Daten',

    metadataHeadline: 'Metadaten:',

    detailViewTitleUpload: 'Daten hochladen:',
    detailViewTitleEdit: 'Editieren:',
    detailViewSelectLabel: 'Einträge auswählen:',
    detailViewBadFileName: 'Ungültiger Dateiname. Folgende Zeichen sind nicht zulässig: \/:?*<>|',

    detailViewBtnCancel: 'Abbrechen',
    detailViewBtnUpload: 'Hochladen',
    detailViewBtnSelectFiles: 'Dateien auswählen',

    detailViewBtnDelete: 'Löschen',
    detailViewBtnDownload: 'Herunterladen',
    detailViewBtnReuse: 'Wiederverwenden',
    detailViewBtnMove: 'Verschieben',
    detailViewBtnPublish: 'Veröffentlichen',
    detailViewBtnArchive: 'Archivieren',
    detailViewBtnUpdate: 'Aktualisieren',

    infoFileType: 'Dateiart',
    infoFileTypeFolder: 'Ordner',
    infoFileTypeFile: 'Dateien',
    infoFileName: 'Dateinamen',
    infoFileAbsolutePath: 'Dateipfad',
    infoFileLastModified: 'Zuletzt geändert',
    infoFileCreated: 'Erstellt',
    infoFileSize: 'Dateigröße',
    infoFileContent: 'Inhalt',
    infoFileFiles: 'Dateien',
    infoFileNoInformation: 'Keine Information',
    errorLoadingResource: 'Die Ressource konnte nicht geladen werden.',
    tableIsEmpty: 'Es existieren aktuell noch keine Entitäten für diese Ressource.',

    multiselectPlaceholder: 'Bitte auswählen!',
};
