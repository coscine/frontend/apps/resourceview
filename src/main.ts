import jQuery from 'jquery';
import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import ResourceViewApp from './ResourceViewApp.vue';
import locales from './locale/locales';
import VueI18n from 'vue-i18n';
import { LanguageUtil } from '@coscine/app-util';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueI18n);

jQuery(() => {
  const i18n = new VueI18n({
    locale: LanguageUtil.getLanguage(), // set locale
    messages: locales, // set locale messages
    silentFallbackWarn: true,
  });

  new Vue({
    render: (h) => h(ResourceViewApp),
    i18n,
  }).$mount('resourceview');
});
