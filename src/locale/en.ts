export default {
    resources: 'Resources',

    upload: 'Upload',
    download: 'Download',

    canDropFile: 'You can drop your file here!',

    loading: 'Loading...',
    typeToSearch: 'Type to Search',
    clear: 'Clear',
    perPage: 'Per page',
    more: 'More',
    fileName: 'File Name',
    lastModified: 'Last Modified',
    actions: 'Actions',

    noData: 'This Resource contains no Data.',

    metadataHeadline: 'Metadata:',

    detailViewTitleUpload: 'Upload Data:',
    detailViewTitleEdit: 'Edit:',
    detailViewSelectLabel: 'Selected Entry(s):',
    detailViewBadFileName: 'Invalid file name. The following characters are not permissible: \/:?*<>|',

    detailViewBtnCancel: 'Cancel',
    detailViewBtnUpload: 'Upload',
    detailViewBtnSelectFiles: 'Select Files',

    detailViewBtnDelete: 'Delete',
    detailViewBtnDownload: 'Download',
    detailViewBtnReuse: 'Reuse',
    detailViewBtnMove: 'Move',
    detailViewBtnPublish: 'Publish',
    detailViewBtnArchive: 'Archive',
    detailViewBtnUpdate: 'Update',

    infoFileType: 'File Type',
    infoFileTypeFolder: 'Folder',
    infoFileTypeFile: 'File',
    infoFileName: 'File Name',
    infoFileAbsolutePath: 'File Path',
    infoFileLastModified: 'Last Modified',
    infoFileCreated: 'Created',
    infoFileSize: 'File Size',
    infoFileContent: 'Content',
    infoFileFiles: 'Files',
    infoFileNoInformation: 'No Information',
    errorLoadingResource: 'The Resource could not be loaded.',
    tableIsEmpty: 'There are currently no entities for this resource.',

    multiselectPlaceholder: 'Please select!',
};
